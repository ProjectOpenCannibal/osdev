CINCLUDE := src/include

CFLAGS := -m32 -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -nostdinc -O -g -Wall -I$(CINCLUDE)
LDFLAGS := -nostdlib -Wl,-N -Wl,-Ttext -Wl,100000

all: kernel.bin

kernel.bin:	setup
				nasm -f aout -o obj/start.o src/start.asm
				gcc $(CFLAGS) -c -o obj/main.o src/main.c
				gcc $(CFLAGS) -c -o obj/screen.o src/screen.c
				gcc $(CFLAGS) -c -o obj/gdt.o src/gdt.c
				gcc $(CFLAGS) -c -o obj/idt.o src/idt.c
				gcc $(CFLAGS) -c -o obj/isrs.o src/isrs.c
				gcc $(CFLAGS) -c -o obj/irq.o src/irq.c
				gcc $(CFLAGS) -c -o obj/timer.o src/timer.c
				gcc $(CFLAGS) -c -o obj/keyboard.o src/keyboard.c
				ld -m elf_i386 -T src/link.ld -o out/kernel.bin \
					obj/start.o \
					obj/main.o \
					obj/screen.o \
					obj/gdt.o \
					obj/idt.o \
					obj/isrs.o \
					obj/irq.o \
					obj/timer.o \
					obj/keyboard.o
					
				@echo Done!
				
kernel2.bin: setup start.o main.o screen.o
				gcc -o out/kernel.bin $(CFLAGS) start.o main.c screen.c $(LDFLAGS)

setup:
				mkdir obj
				mkdir out

clean:
				rm -rf obj
				rm -rf out
